local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local execute = vim.api.nvim_command
local venvs = fn.stdpath("data") .. "/virtualenvs"
local cmds = {
    "!mkdir -p " .. venvs,
    "cd " .. venvs,
    "python3.9 -m venv debugpy",
    "debugpy/bin/python -m pip install debugpy",
}
print(table.concat(cmds, " && "))
